# group-template

## Foreword

Hello!

This is an example of a group blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each group has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/groups) in which a template of a blog is already stored. 

The GitLab software is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

You can [install and run MkDocs](https://www.mkdocs.org/getting-started/) on your computer to work on a local version of your website. MkDocs comes with a built-in dev-server that lets you preview your documentation as you work on it using the 'mkdocs serve' command.

## Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.

## Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

## It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.