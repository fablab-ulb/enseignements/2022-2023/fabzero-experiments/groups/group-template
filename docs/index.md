# Group and project presentation

## About our team

Our team is composed of :

- [Arsène Lupin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/template/)
- [Sherlock Holmes](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/template/)
- [Hercule Poirot](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/template/)
- [Al Capone](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/template/)
- [Columbo](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/template/)

Here we are :

![](images/avatar-group-photo.jpg)

Describe your team !

## Our Project abstract

Describe here a small summary of your project.

![Graphical abstract](images/firstname.surname-slide.png)


